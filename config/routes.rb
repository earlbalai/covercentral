Covercentral::Application.routes.draw do
  devise_for :users

  #Root
  root :to => 'pages#index'

end
