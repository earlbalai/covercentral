module ApplicationHelper

  # Public: Sets the yield(:title) block, for use in application.html.erb
  #   page_title - String for the page title
  #
  # Returns nothing.
  def title(page_title, show_title = true)
    content_for(:title) { h(page_title.to_s) }
    @show_title = show_title
  end

  # Public: Was the title set to be displayed?
  #
  # Returns boolean.
  def show_title?
    @show_title
  end

  # Public: Returns active if the current_page is the path.
  #   page_path - Path of page which we're looking to see if is active.
  #
  def page_is_active?(page_path)
    'active' if current_page?(page_path)
  end

end
